/*** Declaración de Variables ***/
// Javascript es de tipado inferido

let nombre = 'Grover' //👈 eso es inferencia, quiere decir q en ts no es necesario definir el tipo pero si es recomendable
    , email: string = 'grover@gmail.com'
console.log(`El email de ${nombre} es: ${email}`)

// constante
const PI: number = 3.1416

let apellidos: any = 'Cristobal'
let error: boolean = false

/*** Instanciación Múltiple de Variables ***/

let a: string, b: boolean, c: number; // instancia 3 variables sin valor inicial
a = 'hola';
b = true;
c = 5;

/*** Tipos más complejos ***/

// Lista de cadenas de texto
const listas: string[] = ['a', 'b'];

// Combinación de tipos en listas
const valores: (string | number | boolean)[] = [true, 'Hola', false, 56]

/*** Enumerados ***/

enum Estados {  // los enum empiezan en 0 y si se agrega un valor, los sgtes se incrementan, y tmb podemos agregar letras
    'Completado' = 'C',
    'Incompleto' = 'I',
    'Pendiente' = 'P',
}
enum PuestoCarrera {
    'Primero' = 1,
    'Segundo',
    'Tercero',
}
let estadoTarea: Estados = Estados.Incompleto;
console.log(estadoTarea); // I

let estadoCarrera: PuestoCarrera = PuestoCarrera.Segundo;
console.log(estadoCarrera); // 2

/*** Interfaces ***/

interface Tarea {
    nombre: string;
    estado: Estados;
    prioridad: number;
}

// podemos crear variables que sigan la interface Tarea
let tarea1: Tarea = {
    nombre: 'Tarea 1',
    estado: Estados.Incompleto,
    prioridad: 10,
};
console.log(tarea1);

/*** Asignación Múltiple de Variables ***/

let miTarea = {
    titulo: 'Mi tarea',
    estado: Estados.Completado,
    prioridad: 1
}

// Declaración 1 a 1
let miTitulo = miTarea.titulo
let miEstado = miTarea.estado
let miPrioridad = miTarea.prioridad

/*** Factor Spread (Propagación) ***/

// En asignación de variables
let {titulo, estado, prioridad} = miTarea

// En listas
let listaCompraLunes: string[] = ['Azúcar', 'Arroz']
let listaCompraMartes: string[] = [...listaCompraLunes, 'Pan', 'Huevos']
let listaCompraMiercoles: string[] = ['Carne','Pescado']
let listaCompraSemana: string[] = [...listaCompraMartes, ...listaCompraMiercoles]

// En Objetos
let estadApp = {
    usuario: 'admin',
    session: 3,
    jwt: 'Bearer5942893654'
}

// cambia un valor por propagación
let nuevoEstado = {
    ...estadApp,
    session: 4
}

/*** Types de TypeScript ***/
// La diferencia entre Types e Interfaces es que Types crea un nuevo Tipo de Dato y Interfaces crea un nuevo Objeto

type Producto = {
    nombre: string;
    precio: number;
    anio: number
};

let coche: Producto = {
    nombre: "BMW",
    precio: 65000,
    anio: 2010
};

/*** Condicionales ***/

// Operadores ternarios
console.log(`${coche.anio > 2010 ? "El coche es nuevo" : "El coche es viejo"}`);

// Switch
switch (tarea1.estado) {
    case Estados.Completado:
        console.log('La tarea está completada')
        break;
    case Estados.Incompleto:
        console.log('La tarea no está completada')
        break;
    case Estados.Pendiente:
        console.log('La tarea está pendiente de comprobarse')
        break;
    default:
        break;
}

/*** Bucles ***/

const listaTareasNuevas: Tarea[] = [
    {
        nombre: 'Programar en JS',
        estado: Estados.Incompleto,
        prioridad: 10
    },
    {
        nombre: 'Programar en React',
        estado: Estados.Pendiente,
        prioridad: 2
    },
    {
        nombre: 'Programar en TypeScript',
        estado: Estados.Completado,
        prioridad: 5
    }
]

// forEach
listaTareasNuevas.forEach((tarea: Tarea, index: number) => {
    console.log(`${index} - ${tarea.nombre}`)
})

// Bucles while
while (tarea1.estado !== Estados.Completado) {
    if (tarea1.prioridad == 20) {
        tarea1.estado = Estados.Completado
        break
    } else {
        tarea1.prioridad++
    }
}

// DO while (se ejecuta al menos una vez)
do {
    tarea1.prioridad++
    tarea1.estado = Estados.Completado
} while(tarea1.estado !== Estados.Completado)